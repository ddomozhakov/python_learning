from xml.etree import ElementTree

depth = 1
colors = {"red": 0, "green": 0, "blue": 0}

root = ElementTree.parse("colors.xml").getroot()

def color_count(el, dep):
    colors[el.attrib['color']] += dep
    dep += 1
    for elem in el:
        color_count(elem, dep)

color_count(root, depth)
print(colors)
