import requests, sys, re

buff = []
aset = set()
with open('urlfile.html') as infile:
    for line in infile:
        line = line.strip()
        buff.append(re.findall(r"(?:<a .*href=)(?:[\"\'])(?:.*://)?([\w*.-]*)", line))

for bu in buff:
    if len(bu) > 0:
        if bu[0] != '..':
            aset.add(bu[0])

buff = []
for ase in aset:
    buff.append(ase)
buff.sort()
for bu in buff:
    print(bu)
