import json

inher = dict()
js = '[{"name": "B", "parents": ["A", "C"]}, {"name": "C", "parents": ["A"]}, {"name": "A", "parents": []}, {"name": "D", "parents":["C", "F"]}, {"name": "E", "parents":["D"]}, {"name": "F", "parents":[]}]'
# js = '[{"name": "A", "parents": []}, {"name": "B", "parents": ["A", "C"]}, {"name": "C", "parents": ["A"]}]'
ps = json.loads(js)
values = dict()

def req_inher(item, parent):
    if item == parent:
        return True
    else:
        for par in inher[item]:
            if par == "obj":
                break
            if par == parent:
                return True
            else:
                if req_inher(par, parent):
                    return True

for dicts in ps:
    inher[dicts['name']] = dicts['parents']
    values[dicts['name']] = 0

for na in inher:
    # print(na)
    # print(req_inher('C', na))
    for chi in values:
        if req_inher(chi, na):
            values[na] += 1

for key in sorted(values):
    print(key,":", values[key])
