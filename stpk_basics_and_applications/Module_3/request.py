import requests, sys, re
url0 = []
url1 = []
fl = False
for line in sys.stdin:
    url0.append(line.rstrip())
res0 = requests.get(url0[0])
urls = re.findall(r"ht.+l", str(res0.content))
for ur in urls:
    res = requests.get(ur)
    A = str(res.content)
    B = re.findall(r"ht.+l", A)
    for i in B:
        if url0[1] == i:
            fl = True
if fl:
    print("Yes")
else:
    print("No")