inher = dict()
exc = []

def make_inher(item, parents):
    inher[item] = parents

def req_inher(item, parent):
    if item == parent:
        return True
    else:
        for par in inher[item]:
            if par == "obj":
                break
            if par == parent:
                return True
            else:
                if req_inher(par, parent):
                    return True

with open('excep_make.dat') as inf:
    for line in inf:
        line = line.split('\n')
        commands = line[0].split(' ')
        if len(commands) == 1:
            make_inher(commands[0], ["obj"])
        else:
            make_inher(commands[0], commands[2:])

# print(inher)

with open('excep_request.dat') as inf:
    for line in inf:
        flag = False
        line = line.split('\n')
        exc.append(line[0].split(' ')[0])
        if len(exc) > 1:
            for i in range(0, (len(exc) - 1)):
                if req_inher(exc[-1], exc[i]):
                    flag = True
        if flag:
            print(exc[-1])
