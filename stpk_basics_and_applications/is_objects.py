import timeit

def fib1(x):
    if x == 0 or x == 1:
        return 1
    else:
        return fib1(x - 1) + fib1(x - 2)

def fib2(x):
    pref1 = fib1(x - 2)
    pref2 = pref1 + fib1(x - 3)
    return pref1 + pref2

def fib2_v(x):
    cur = 0
    prev = 0
    fib_vikusha(x, cur, prev)
    return prev + cur

def fib_vikusha(x, cur, prev):
    if x == 0 or x == 1:
        prev = 0
        cur = 1
        return
    fib_vikusha(x-1, cur, prev)
    prev = cur
    cur = x


data = 30

time1 = timeit.timeit("fib1(data)", setup="from __main__ import fib1, data", number=1)
time2 = timeit.timeit("fib2(data)", setup="from __main__ import fib2, data", number=1)

print('Черепаха лажа писос', time1)
print('Гонщик круто нравится', time2)
print('Ээ смотри как хорошо стало, а!!!', time1/time2)

# print(fib1(11))
# print(fib2(11))



print(fib2_v(data))