inher = dict()

def make_inher(item, parents):
    inher[item] = parents

def req_inher(item, parent):
    if item == parent:
        return True
    else:
        for par in inher[item]:
            if par == "obj":
                break
            if par == parent:
                return True
            else:
                if req_inher(par, parent):
                    return True

with open('inher_make.dat') as inf:
    for line in inf:
        line = line.split('\n')
        commands = line[0].split(' ')
        if len(commands) == 1:
            make_inher(commands[0], ["obj"])
        else:
            make_inher(commands[0], commands[2:])



with open('inher_request.dat') as inf:
    for line in inf:
        line = line.split('\n')
        commands = line[0].split(' ')
        if req_inher(commands[1], commands[0]):
            print("Yes")
        else:
            print("No")
