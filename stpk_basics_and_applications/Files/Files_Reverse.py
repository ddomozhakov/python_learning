buff = []

with open('in.dat') as inf:
    for line in inf:
        line = line.strip()
        buff.append(line)



with open('out.dat', 'a') as outf:
    for i in range(len(buff)):
        outf.write(buff.pop())
        outf.write("\n")



# lines = open("input.txt").readlines()
# with open("output.txt", "w") as out:
#     out.writelines(reversed(lines))