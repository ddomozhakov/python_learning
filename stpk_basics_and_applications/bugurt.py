# x = [x*2 for x in range(10) if x % 2 == 0]
#
# def sum(x):
#     return x
#
# # y = (sum(x) for i in x)
# y = (lambda z: z, x)
#
# # print(next(y))
# print(y[1])

import operator as op

print(op.add(1,2))



s, t, count = [input() for i in range(2)] + [0]
for i in range(len(s)):
    if s[i:].startswith(t):
        count += 1
print(count)