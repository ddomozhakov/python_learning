import pandas as pd

category_columns = { col: 'category' for col in ['Survived', 'Sex'] }  # приведение выбранных колонок к категориальному
# print(category_columns)
data = pd.read_csv('https://stepic.org/media/attachments/course/524/train.csv', dtype=category_columns)
print(data)


