from scipy.stats import chi2_contingency
from statsmodels.graphics.mosaicplot import mosaic
from scipy.stats import chisquare
import matplotlib.pyplot as plt

# A C G T

V1 = [0, 0, 0, 0]
V2 = [0, 0, 0, 0]
V3 = [0, 0, 0, 0]

with open('test_data.csv') as inf:
    inf.readline()
    for line in inf:
        line = line.strip()
        if line[1] == "A":
            V1[0] += 1
        elif line[1] == "C":
            V1[1] += 1
        elif line[1] == "G":
            V1[2] += 1
        elif line[1] == "T":
            V1[3] += 1
        if line[5] == "A":
            V2[0] += 1
        elif line[5] == "C":
            V2[1] += 1
        elif line[5] == "G":
            V2[2] += 1
        elif line[5] == "T":
            V2[3] += 1
        if line[9] == "A":
            V3[0] += 1
        elif line[9] == "C":
            V3[1] += 1
        elif line[9] == "G":
            V3[2] += 1
        elif line[9] == "T":
            V3[3] += 1

print("")
print("V1", V1)
print("V2", V2)
print("V3", V3)
print("")
mosaic([V1, V2, V3], gap=0.01)
# plt.show()
print("Common check:", chi2_contingency([V1, V2, V3])[1])
print("A check:", chisquare([V1[0], V2[0], V3[0]])[1])
print("B check:", chisquare([V1[1], V2[1], V3[1]])[1])
print("C check:", chisquare([V1[2], V2[2], V3[2]])[1])


# plot histogram for nums of count list