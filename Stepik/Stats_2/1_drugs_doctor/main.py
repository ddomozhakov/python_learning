from scipy.stats import chi2_contingency
from statsmodels.graphics.mosaicplot import mosaic
from scipy.stats import chisquare
import matplotlib.pyplot as plt

V1 = [0, 0]
V2 = [0, 0]
V3 = [0, 0]

with open('test_drugs.csv') as inf:
    inf.readline()
    for line in inf:
        line = line.strip().split(',')
        if line[0] == '"drug_1"' and line[1] == '"negative"':
            V1[0] += 1
        elif line[0] == '"drug_1"' and line[1] == '"positive"':
            V1[1] += 1
        elif line[0] == '"drug_2"' and line[1] == '"negative"':
            V2[0] += 1
        elif line[0] == '"drug_2"' and line[1] == '"positive"':
            V2[1] += 1
        elif line[0] == '"drug_3"' and line[1] == '"negative"':
            V3[0] += 1
        elif line[0] == '"drug_3"' and line[1] == '"positive"':
            V3[1] += 1

print("")
print("V1", V1)
print("V2", V2)
print("V3", V3)
print("")
mosaic([V1, V2, V3], gap=0.01)
# plt.show()
print("Common check:", chi2_contingency([V1, V2, V3]))
print("A check:", chisquare([V1[0], V1[1]])[1] )
print("B check:", chisquare([V2[0], V2[1]])[1])
print("C check:", chisquare([V3[0], V3[1]])[1])

Neg = [V1[0], V2[0], V3[0]]
Pos = [V1[1], V2[1], V3[1]]

print("Neg", Neg)
print("Pos", Pos)

print("Common check:", chi2_contingency([Neg, Pos]))
print("")
# print(chisquare([290, 424]))
# print(2.7**(-0.8472979))

p = 2.718**(-0.8472979) / (1 + 2.718**(-0.8472979))
print(p*50)

