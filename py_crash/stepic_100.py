in_list = []
classes = {}
co_aver = 0
aver_dl = 0
classes_out = {}
with open('step_100.dat') as inf:
    for line in inf:
        if line.split()[0] not in classes:
            classes[line.split()[0]] = [line.split()[2]]
        elif line.split()[0] in classes:
            classes[line.split()[0]].append(line.split()[2])
    for key in classes:
        for dl in classes[key]:
            aver_dl += int(dl)
            co_aver += 1
        aver_dl = aver_dl/co_aver
        co_aver = 0
        classes[key] = aver_dl
        aver_dl = 0

for i in range(1, 12):
    if str(i) in classes:
        print(i, classes[str(i)])
    else:
        print(i, '-')


