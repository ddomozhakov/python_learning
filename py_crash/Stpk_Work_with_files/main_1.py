with open('i_1.dat') as inf:
    in_list = inf.readline()

out_list = ''
curr_amount = 0
curr_int_st = ''
curr_char = ''

def isInt(st):
    return('0' <= st <= '9')

for chi in range(len(in_list) - 1, -1, -1):
    print(chi)
    if isInt(in_list[chi]):
        curr_int_st = in_list[chi] + curr_int_st
    else:
        curr_char = in_list[chi]
        out_list = str(in_list[chi]) * int(curr_int_st) + out_list
        curr_int_st = ''


print(in_list)
print(out_list)

with open('o_1.dat', 'w') as ouf:
    ouf.write(out_list)


