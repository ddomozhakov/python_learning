"""
for i in range(1,21):
    print(i**2)

for j in range(1,int(1e3)):
    print(j)

million = []
for k in range(1, int(1e6)+1):
    million.append(k)

print(min(million))
print(max(million))
print(sum(million))

odd_num = list(range(1,21, 2))
print(odd_num)
for i in odd_num:
    print(i)

mult_of_3 = list(range(3,31,3))
for th in mult_of_3:
    print(th)
"""

cubes = [value**3 for value in range (1,11)]
print(cubes)

