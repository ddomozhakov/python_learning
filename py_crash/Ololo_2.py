ntstr_i = ''
arr = []
x = 0
y = 0
while True:
    str_i = input()
    if str_i == 'end':
        break
    ai = str_i.split()
    y = len(ai)
    arr.append(ai)
    x = x + 1
arr = [list(map(int, i)) for i in arr]

sum = [[0 for j in range(y)] for i in range(x)]

for i in range(x):
    for j in range(y):
        if 1 <= i < x - 1 and 1 <= j < y - 1:
            sum[i][j] = arr[i - 1][j] + arr[i + 1][j] + arr[i][j - 1] + arr[i][j + 1]
        elif i == 0 and j < y - 1:
            sum[i][j] = arr[x - 1][j] + arr[i + 1][j] + arr[i][j - 1] + arr[i][j + 1]
        elif i == x - 1 and j < y - 1:
            sum[i][j] = arr[i - 1][j] + arr[0][j] + arr[i][j - 1] + arr[i][j + 1]
        elif 1 <= i < x - 1 and j == 0:
            sum[i][j] = arr[i - 1][j] + arr[i + 1][j] + arr[i][y - 1] + arr[i][j + 1]
        elif 1 <= i < x - 1 and j == y - 1:
            sum[i][j] = arr[i - 1][j] + arr[i + 1][j] + arr[i][j - 1] + arr[i][0]
        elif i == 0 and j == 0:
            sum[i][j] = arr[x - 1][j] + arr[i + 1][j] + arr[i][y - 1] + arr[i][j + 1]
        elif i == x - 1 and j == y - 1:
            sum[i][j] = arr[i - 1][j] + arr[0][j] + arr[i][j - 1] + arr[i][0]
        elif i == 0 and j == y - 1:
            sum[i][j] = arr[x - 1][j] + arr[i + 1][j] + arr[i][j - 1] + arr[i][0]
        elif i == x - 1 and j == 0:
            sum[i][j] = arr[i - 1][j] + arr[0][j] + arr[i][y - 1] + arr[i][j + 1]

for i in range(0, y):
    for j in range(0, x):
        print(sum[i][j], end=' ')
    print('\n')

# print(sum)


# print(sum)


''''''
9 5 3
0 7 -1
-5 2 9

3 21 22
10 6 19
20 16 -1
''''''