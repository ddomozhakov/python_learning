current_users = ['Mike', 'Barbara', 'Anon', 'Opa', 'Pahom']
new_users = ['John', 'Barbara', 'Anon', 'Pepe', 'Bratan']

for name in new_users:
    if name in current_users:
        print("Hi,", name, ", you are there!")
    else:
        print("How you doing,", name, "?")
